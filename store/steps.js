// We've to disable param reassign, because it's the common behavior of vuex
/* eslint-disable no-param-reassign */
import sortBy from 'lodash.sortby';

export const STEP_CHANGE_CAR = 'ChangeCar';
export const STEP_GET_PASSENGER = 'GetPassenger';
export const STEP_WAIT_PASSENGER = 'WaitPassenger';
export const STEP_DRIVE_PASSENGER = 'DrivePassenger';
export const STEP_DROP_PASSENGER = 'DropPassenger';
export const STEP_GO_TO_STOP = 'GoToStop';

const stepsOrder = [
  STEP_DRIVE_PASSENGER,
  STEP_DROP_PASSENGER,
  STEP_CHANGE_CAR,
  STEP_GET_PASSENGER,
  STEP_WAIT_PASSENGER,
  STEP_GO_TO_STOP,
];

const RIDE = 'ride';
const SHUTTLE = 'shuttle';

export const state = () => ({
  steps: [],
  alert: false,
});

export const mutations = {
  setSteps: (s, steps = []) => {
    s.steps = steps.filter(({ id }) => !!id);
  },
  upsertStep: (s, step) => {
    const i = s.steps.findIndex((st) => st.id === step.id);
    if (i === -1) {
      s.steps.push(step);
    } else {
      s.steps.splice(i, 1, step);
    }
  },
  deleteStep: (s, step) => {
    const i = s.steps.findIndex((st) => st.id === step.id);
    if (i !== -1) {
      s.steps.splice(i, 1);
    }
  },
  cleanSteps: (s, { id, type }) => {
    s.steps = s.steps.filter((step) => !step[type] || step[type].id !== id);
  },
  toggleAlert: (s) => {
    s.alert = !s.alert;
  },
};

export const getters = {
  steps: (s) => sortBy(
    s.steps
      .filter((step) => !step.done)
      .map((step) => ({ ...step, actionIndex: stepsOrder.indexOf(step.action) })),
    ['date', 'actionIndex'],
  ),
  hasSteps: (s) => s.steps.filter((step) => !step.done).length > 0,
  alert: (s) => s.alert,
};

export const actions = {
  async fetchSteps({ commit }, { campus, after, before }) {
    let res;
    try {
      res = await this.$api.query('steps')
        .setCampus(campus)
        .setDriver(this.$auth.user.id)
        .setMask(
          [
            'id',
            'action',
            'date',
            'createdAt',
            'location(label,id)',
            'car',
            'ride(id,passengersCount,luggage,phone,car,comments)',
            'shuttle',
            'done',
            'type',
          ],
        )
        .list()
        .setFilters({
          after, before,
        });
    } catch (e) {
      throw new Error('Steps fetching failed');
    }

    const steps = res.data.map((step) => {
      if (step.type === RIDE) {
        delete step.shuttle;
      } else {
        delete step.ride;
      }
      return step;
    });

    commit('setSteps', steps);
  },

  stepsUpdate: ({ commit, rootState }, steps) => {
    const { id } = rootState.auth.user;
    steps.forEach((step) => {
      const displacementName = step.ride ? RIDE : SHUTTLE;
      if (
        step[displacementName]
        && step[displacementName].driver
        && step[displacementName].driver.id === id
      ) {
        commit('upsertStep', step);
      } else {
        commit('deleteStep', step);
      }
    });
  },
};
