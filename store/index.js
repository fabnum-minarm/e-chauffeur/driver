// We've to disable param reassign, because it's the common behavior of vuex
/* eslint-disable no-param-reassign */

export const state = () => ({
  isReconnecting: false,
  isMenuOpen: false,
});

export const mutations = {
  reconnecting: (s, reconnecting = true) => {
    s.isReconnecting = reconnecting;
  },
  toggleMenu: (s, bool) => {
    s.isMenuOpen = typeof bool !== 'undefined' ? bool : !s.isMenuOpen;
  },
};

export const getters = {
  isReconnecting: (s) => s.isReconnecting,
  isMenuOpen: (s) => s.isMenuOpen,
};

export const actions = {
  socket_stepsUpdate({ dispatch }, payload) {
    dispatch('steps/stepsUpdate', payload);
  },
  socket_cleanSteps({ commit }, payload) {
    commit('steps/cleanSteps', payload);
  },
  reconnecting({ commit }, reconnecting = true) {
    commit('reconnecting', reconnecting);
  },
};
