# eChauffeur driver app
[![pipeline status](https://gitlab.com/fabnum-minarm/e-chauffeur/driver/badges/develop/pipeline.svg)](https://gitlab.com/fabnum-minarm/e-chauffeur/driver/-/commits/develop)

> eChauffeur driver application

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```
